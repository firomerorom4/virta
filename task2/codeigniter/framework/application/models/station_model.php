<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 18/01/2018
 * Time: 3:18 AM
 */

class Station_model extends CI_Model
{
    // Constructor
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * @param $station
     * @return array
     */
    public function get_station_id($station)
    {
        $this->db->where('id', $station);
        $query = $this->db->get('station', 1);

        if ($query->num_rows() == 1)
        {
            return $query->row_array();
        }
        else
            return array();
    }


    /**
     * Return all stations
     * @return array
     */
    public function get_stations()
    {
        $this->db->order_by('id','asc');
        $query = $this->db->get('station');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }

    /**
     * keys name, latitude, longitude, company_id
     * @param array $data
     * @param $id
     * @return bool
     */
    public function set_station(array $data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('station', $data);
        return true;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function insert_station(array $data)
    {
        $this->db->insert('station', $data);
        $id_product = $this->db->insert_id();
        return $id_product;
    }

    /**
     * @param $id
     */
    public function delete_station($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('station');
    }

}