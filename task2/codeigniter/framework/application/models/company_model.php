<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 21/01/2018
 * Time: 11:55 PM
 */


class Company_model extends CI_Model{
    // Constructor
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * @param $company
     * @return array
     */
    public function get_company_id($company)
    {
        $this->db->where('id', $company);
        $query = $this->db->get('company', 1);

        if ($query->num_rows() == 1)
        {
            return $query->row_array();
        }
        else
            return array();
    }


    /**
     * Return all companys
     * @return array
     */
    public function get_companies()
    {
        $this->db->order_by('id','asc');
        $query = $this->db->get('company');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }

    /**
     * keys name, latitude, longitude, company_id
     * @param array $data
     * @param $id
     * @return bool
     */
    public function set_company(array $data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('company', $data);
        return true;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function insert_company(array $data)
    {
        $this->db->insert('company', $data);
        $id_product = $this->db->insert_id();
        return $id_product;
    }

    /**
     * @param $id
     */
    public function delete_company($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('company');
    }

}